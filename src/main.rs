use std::process;

fn main() {
    if let Err(e) = tstk_rs::run() {
        println!("Error: {}", e);

        process::exit(1);
    }
}
